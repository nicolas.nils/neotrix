<?php
/*
Neonix
*/
//OUverture en trhead multiple soxket 

//pkill -f start.php
declare(strict_types = 1);
declare(ticks = 1);

shell_exec('clear');

error_reporting(E_ALL);
ini_set('memory_limit', '100M');

//ini_set('error_reporting', E_ALL ^ E_NOTICE);
//ini_set('display_errors', 1);

// Set time limit to indefinite execution
set_time_limit(0);



echo PHP_EOL;
echo "------------------------------------------------------". PHP_EOL;;
echo ' '. __FILE__                                           . PHP_EOL;
echo '          MultiThreading No Blocking Programme        '. PHP_EOL;
echo '                                                      '. PHP_EOL;
echo ' v0.1 - 12/03/2023                            Neonix© '. PHP_EOL;
echo "------------------------------------------------------". PHP_EOL;
echo PHP_EOL;
echo 'Bonjour '.$GLOBALS["_SERVER"]["LOGNAME"] . PHP_EOL;
echo PHP_EOL;



/*
pcntl_signal(SIGTERM, "sig_handler");
pcntl_signal(SIGHUP,  "sig_handler");
pcntl_signal(SIGUSR1, "sig_handler");


pcntl_signal(SIGUSR1, function ($signal) {

            echo 'HANDLE SIGNAL ' . $signal . PHP_EOL;

});
posix_kill(posix_getpid(), SIGUSR1);
pcntl_async_signals(true);
pcntl_signal(SIGUSR1,function(){echo "received SIGUSR1";});
pcntl_signal(SIGUSR2,function(){echo "received SIGUSR2";});
posix_kill(posix_getpid(),SIGUSR1);
posix_kill(posix_getpid(),SIGUSR2);
*/






$stop = false;
$processList = [];



/**
 * Starts the work
 */
function launch(): void
{
    global $stop, $processList;

    $processMax     = 5; //Noombre de process bloquant MAX
    $processCount   = 0;
    $status         = null;

    echo 'Running as pid: ' . getmypid() . PHP_EOL;
    $processList[] = getmypid();
    $taskList = generator();


    do {
        //echo 'Still tasks to do' . PHP_EOL;

        if ($processCount >= $processMax) {
            echo 'Waiting, currently running: ' . $processCount . PHP_EOL;
            pcntl_wait($status);
            $processCount--;
            continue;
        }


        echo 'Tasks running: ' . $processCount . PHP_EOL;     
        $task = $taskList->current();
        $taskList->next();
        $processCount++;
        $pid = pcntl_fork();

        if ($pid === -1) {
            exit('Error forking...' . PHP_EOL);
        }

        if ($pid === 0) {
            $processList[] = getmypid();
            echo 'Running task '.$task.' as pid: ' . getmypid() . PHP_EOL;
            $callbackTaskId = execute_task($task);
            $processList[] = $callbackTaskId;
            exit();
        }

        $processList[] = $pid;



        //var_dump($processList);



    } while ($taskList->valid());



    $waitForChildrenToFinish = true;



    while ($waitForChildrenToFinish) {
        // This while loop holds the parent process until all the child threads
        // are complete - at which point the script continues to execute.
        if (pcntl_waitpid(0, $status) === -1) {
            echo PHP_EOL;
            echo 'Parallel execution is complete' . PHP_EOL;
            $waitForChildrenToFinish = false;
        }
    }



    // Code to run after all tasks are processed
}

/**
 * Helper method to execute a task
 */

function execute_task(int $task_id): int
{

    global $stop, $processList;
    $time_start = microtime(true);
    //global $address;
    echo 'Starting task: ' . $task_id . PHP_EOL;


    //MODULE
    //  1 Terminal
    //  2 HTTP serveur
    //  3 Random sleep xD

    if($task_id == 1)
    {
        //Console terminal ligne de command
        //strtolower($key)
        //Insertion module bloquant
        require(dirname(__FILE__)  . '/module/' . 'http' . '.php');  
        
    }
    elseif($task_id == 2) {
        require(dirname(__FILE__)  . '/module/' . 'https' . '.php');        
    }

    elseif($task_id == 3) {
        require(dirname(__FILE__)  . '/module/' . 'terminal' . '.php');        
    }
    else {
        //Insertion module bloquant
        include(dirname(__FILE__)  . '/module/' . 'sleep' . '.php');
    }
    echo "Completed task: ".$task_id." Took ".$execution_time ." seconds.\n";

    return $task_id;
}


/**
 * Builds a list of tasks
 */
function generator(): Generator
{
    global $stop, $processList;

    $item_count = 10;
    for ($i = 1; $i <= $item_count; $i++) {
        yield $i;
    }
}






launch();
